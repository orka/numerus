define(['p!-numerus/number', 'p!-numerus/error'], function($number, $error) {
    'use strict';
    return {
        number: $number,
        error: $error
    };
});
