/**
 * Simple Class providing API to set retain and express in time a number value
 */
define(['p!-eventful',
    'p!-curves',
    'p!-math',
    'p!-assertful',
    'p!-numerus/error',
    'p!-logger'
], function($eventful,
    $curves,
    $math,
    $assertful,
    $error,
    $logger) {
    'use strict';


    function onCurveStarted() {
        this.emit('curveStart');
    }

    function onCurveEnded() {
        this.emit('curveEnd');
    }

    function onCurveCompleted () {
        this.emit('curveComplete');
    }


    /**
     * Sets event based number value component which stores initial value.
     * @param   {Number} __value any number
     * @param   {Number} __min   a minimum value beyond which value will never be set, default -Infinity
     * @param   {Number} __max   a maximum value beyond which value will never be set, default +Infinity
     * @param   {Object} __curve Curve instance that will be used to curve value over time using curve progression
     * @returns {Numerus} instance of Numerus object
     */
    var Numerus = $eventful.emitter.subclass('Numerus', {

        /**
         * Sets event based number value component which stores initial value.
         * @constructor Numerus
         * @param   {Number} __value any number
         * @param   {Number} __min   a minimum value beyond which value will never be set, default -Infinity
         * @param   {Number} __max   a maximum value beyond which value will never be set, default +Infinity
         * @param   {Object} __curve Curve instance that will be used to curve value over time using curve progression
         */
        constructor: function(__value, __min, __max, __curve) {
            Numerus.super.constructor.call(this);
            //test optional arguments
            if ((!$assertful.optNumber(__min) && __min !== -Infinity) || (!$assertful.optNumber(__max) && __max !== Infinity)) {
                throw 'Max and Min values must be specified as numbers';
            }
            //store original value to enable reset
            this.__orininal__ = __value;
            //if null or undefined - set max or/and min to +-Infinity
            //we should call own APIs to ensure the max/min intersection tests are performed
            this.setMin($assertful.noru(__min) ? -Infinity : __min);
            this.setMax($assertful.noru(__max) ? Infinity : __max);
            //set new value before curve object is set to avoid the curve and have a snap effect instead
            this.set(__value);
            //ensure curves are defined
            //because its optional - attempt to set it only if defined
            if (!$assertful.optObject(__curve)) {
                throw 'Curve must be defined as instance of "curves" package';
            } else if (__curve) {
                this.setCurve(__curve);
            }
        },

        /**
         * Dereferences all objects associated with this class
         */
        deconstructor: function() {
            Numerus.super.deconstructor.call(this);
            this.__curve__ = null;
            this.__min = null;
            this.__max = null;
            this.__value__ = null;
            this.__curveTo__ = null;
            this.__curveFrom__ = null;
            this.__curveDelta__ = null;
        },

        /**
         * Sets the value to initially starting value
         * @param   {Boolean} __bypassCurves if true - then we'll bypass curve if available
         * @returns {Object} this instance
         */
        reset: function(__bypassCurves) {
            this.set(this.__orininal__, __bypassCurves);
            return this;
        },

        /**
         * Sets a min value below which value will not be set
         * If value deeps below min an event "floor" is emitted
         * If the max is specified and is less or equal to the min - error is thrown
         * @param {Number} __min Any number that is less than a max number
         * @returns {Object} this instance
         */
        setMin: function(__min) {
            if (!$assertful.number(__min) && __min !== -Infinity) {
                this.emit('error', $error($error.TYPE.TYPE_ERROR, 'Provided arguments is not a number'));
            }

            this.__min = __min;
            //set a guard against the mismatch of min/max values
            if ($assertful.number(this.__max) && this.__max <= this.__min) {
                $error($error.INVALID_MAX_MIN, 'Maximum value cannot be equal to or less than minimum value specified').throw();
            }
            return this;
        },

        /**
         * Sets a mix value above which value will not be set
         * If value overflows above max an event "ceil" is emitted
         * If the min is specified and is more or equal to the max - error is thrown
         * @param {Number} __max Any number that is more than a min number
         * @returns {Object} this instance
         */
        setMax: function(__max) {
            if (!$assertful.number(__max) && __max !== Infinity) {
                this.emit('error', $error($error.TYPE.TYPE_ERROR, 'Provided arguments is not a number'));
            }

            this.__max = __max;
            //set a guard against the mismatch of min/max values
            if ($assertful.number(this.__min) && this.__min >= this.__max) {
                $error($error.INVALID_MAX_MIN, 'Maximum value cannot be equal to or less than minimum value specified').throw();
            }
            return this;
        },

        /**
         * access to defined min value of the instance
         * @returns    {Number} curent set minimum value
         */
        getMin: function() {
            return this.__min;
        },

        /**
         * access to defined max value of the instance
         * @returns    {Number} curent set maximum value
         */
        getMax: function() {
            return this.__max;
        },

        /**
         * Sets current value to a specified minimum
         * @param   {Boolean} __bypassCurves if true - then we'll bypass curve if available
         * @returns {Object} this instance
         */
        toMin: function(__bypassCurves) {
            this.__set(this.__min, __bypassCurves);
            this.emit('floor');
            return this;
        },

        /**
         * Sets current value to a specified maximum
         * @param   {Boolean} __bypassCurves if true - then we'll bypass curve if available
         * @returns {Object} this instance
         */
        toMax: function(__bypassCurves) {
            this.__set(this.__max, __bypassCurves);
            this.emit('ceil');
            return this;
        },

        /**
         * Assigns a curve object (see package references of curves) that will be used to curve values over time
         * using progression of curve
         * @param {Object} __curve curve object/instance to be used.
         * @returns {Object} this instance
         */
        setCurve: function(__curve) {
            if (!$curves.bezier.isOffspring(__curve)) {
                this.emit('error', $error($error.TYPE.TYPE_ERROR, 'Curve must be defined as instance of "curves" package'));
            } else if (this.__curve__) {
                this.removeCurve();
            }
            this.__curve__ = __curve;
            //create bound methods
            this.onCurveStarted = this.onCurveStarted || onCurveStarted.bind(this);
            this.onCurveEnded = this.onCurveEnded || onCurveEnded.bind(this);
            this.onCurveCompleted = this.onCurveCompleted || onCurveCompleted.bind(this);
            //assign listeners
            this.__curve__.addListener('start', this.onCurveStarted);
            this.__curve__.addListener('end', this.onCurveEnded);
            this.__curve__.addListener('complete', this.onCurveCompleted);
            return this;
        },

        //  addListener: function (__event, __handler) {
        //     if (!this.__curve__) {
        //         Numerus.super.addListener.apply(this, arguments);
        //         return;
        //     }
        //     switch (__event) {
        //         case 'start':
        //         case 'end':
        //         case 'complete':
        //             this.__curve__.addListener(__event, __handler);
        //             break;
        //         default:
        //             Numerus.super.addListener.apply(this, arguments);
        //     }
        // },
        
        /**
         * dereferences currently set curve and its event listeners
         * @returns {Object} this instance
         */
        removeCurve: function() {
            if (!this.__curve__) {
                return this;
            }
            this.__curve__.removeListener('start', this.onCurveStarted);
            this.__curve__.removeListener('end', this.onCurveEnded);
            this.__curve__.removeListener('complete', this.onCurveCompleted);
            this.__curve__ = null;
            return this;
        },

        /**
         * new value setter
         * Checks min/max range and throws error if exceeded
         * @public
         * @param      {number}  __value  The value
         * @param      {Boolean} __bypassCurves if true - bypasses curve set up ending in and seting a new static value
         * @returns {Object} this instance
         */
        set: function(__value, __bypassCurves) {
            //
            if (!$assertful.number(__value) || !$assertful.optBoolean(__bypassCurves)) {
                this.emit('error', $error($error.TYPE.TYPE_ERROR, 'Unable to set value - Not a number or not a Boolean'));
            } else if ($math.nearless(__value, this.__min)) {
                this.toMin(__bypassCurves);
            } else if ($math.nearmore(__value, this.__max)) {
                this.toMax(__bypassCurves);
            } else {
                this.__set(__value, __bypassCurves);
            }

            return this;
        },

        /**
         * Sets a number value
         * If curve is available and a bypass flag was not set to true - then curve is started
         * @private
         * @param   {Number} __value        new value
         * @param   {Boolean} __bypassCurves a curve bypass - if true - we'll ignore the curve
         * and reset the values to a current provided value, same as snap!
         * @returns {Object} this instance
         */
        __set: function(__value, __bypassCurves) {
            if (__bypassCurves !== true && this.__curve__) {
                this.__curve__.start();
                this.__curveTo__ = __value;
                this.__curveFrom__ = this.__value__;
                this.__curveDelta__ = this.__curveTo__ - this.__curveFrom__;
            } else {
                this.__curveTo__ = __value;
                this.__curveFrom__ = __value;
                this.__curveDelta__ = 0;
                this.__value__ = __value;
            }

            return this;
        },

        /**
         * access to the current value
         * @public
         * @returns {Number}  current value
         */
        get: function() {
            this.update();
            return this.__value__;
        },

        /**
         * access to the current end curve value
         * @public
         * @returns {Number}  current value
         */
        getEnd: function() {
            return this.__curveTo__;
        },

        /**
         * access to the current start value
         * @public
         * @returns {Number}  current value
         */
        getStart: function() {
            return this.__curveFrom__;
        },

        /**
         * Updates curve state is specified, else does nothing
         * @returns {Object} this instance
         */
        update: function() {
            if (this.__curve__) {
                this.__curve__.update();
                this.__value__ = this.__curveFrom__ + this.__curveDelta__ * this.__curve__.getLerpedProgression();
            }
            return this;
        },

        /**
         * End curve
         *
         * @returns     {Object}  this instance
         */
        end: function () {
            if (this.__curve__) {
                this.__curve__.end();
            }
            return this;
        }
    });
    return Numerus;
});
