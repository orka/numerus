/**
 * Common error types definitions
 */
define(['p!-error'], function($error) {
    'use strict';

    var type = {
        INVALID_MAX_MIN: 'INVALID_MAX_MIN'
    };

    var ErrorClass = $error.subclass('NumberusError', {
        constructor: function(__type, __message) {
            ErrorClass.super.constructor.apply(this, arguments);
        },
        deconstructor: function() {
            ErrorClass.super.deconstructor.apply(this, arguments);
        },
        TYPE: type
    });

    ErrorClass.TYPE = type;
    return Object.freeze(ErrorClass);
});
