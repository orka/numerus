define(['lib/number'], function($numerus) {
    'use strict';

    describe('When making a Numerus component with optional Min argument', function() {
        var VALUE = Math.random();
        //simple maker closure
        function make() {
            return $numerus.bind(null, VALUE, arguments[0]);
        }

        it('Should not throw if min is null|undefined', function() {
            expect(make()).not.toThrow();
            expect(make(null)).not.toThrow();
        });

        it('Should throw if  min is Function|Object|Array', function() {
            expect(make(make)).toThrow();
            expect(make({})).toThrow();
            expect(make([])).toThrow();
        });

        it('Should throw if value is String|Empty/not', function() {
            expect(make('')).toThrow();
            expect(make('asd')).toThrow();
        });

        it('Should throw if value is a Boolean', function() {
            expect(make(true)).toThrow();
            expect(make(false)).toThrow();
        });

        it('Should throw if value is +Ininity', function() {
            expect(make(Infinity)).toThrow();
        });

        it('Should not throw if value is -Ininity', function() {
            expect(make(-Infinity)).not.toThrow();
        });

        it('Should not throw if value is +-Int or +-Float', function() {
            expect(make(1)).not.toThrow();
            expect(make(1.1)).not.toThrow();
            expect(make(-1)).not.toThrow();
            expect(make(-1.1)).not.toThrow();
            expect(make(0)).not.toThrow();
        });

        it('Should throw if min value is greater or equals to max', function() {
            var MAX = 5;

            function make() {
                return $numerus.bind(null, VALUE, arguments[0], MAX);
            }

            expect(make(MAX)).toThrow();
            expect(make(MAX + 1)).toThrow();
            expect(make(MAX + 1.1)).toThrow();
        });

        it('Should set value to be no less than a set min', function() {
            var value = 111;
            var min = 555; // must be grater than value tfor this spec test
            var instance = $numerus(value, min);
            expect(instance.get()).toEqual(min);
        });
    });


    describe('When accessing Min', function() {
        var instance;
        var VALUE = 666;
        var MIN = 111;
        beforeEach(function() {
            instance = $numerus(VALUE, MIN);
        });

        it('Should be defined', function() {
            expect(instance.getMin).toBeDefined();
        });

        it('Should return initially assigned value', function() {
            expect(instance.getMin()).toEqual(MIN);
        });
    });

    describe('When using "setMin" API', function() {
        var instance;
        var VALUE = 666;
        var MIN = 1;

        function setMin() {
            var _args = arguments;
            return function() {
                instance.setMin.call(instance, _args[0]);
            }
        }

        beforeEach(function() {
            instance = $numerus(VALUE);
        });

        it('Should be defined', function() {
            expect(instance.setMin).toBeDefined();
        });

        it('Should throw if value is null|undefined', function() {
            expect(setMin()).toThrow();
            expect(setMin(null)).toThrow();
        });

        it('Should throw if value is Function|Object|Array', function() {
            expect(setMin(setMin)).toThrow();
            expect(setMin({})).toThrow();
            expect(setMin([])).toThrow();
        });

        it('Should throw if value is String|Empty/not', function() {
            expect(setMin('')).toThrow();
            expect(setMin('asd')).toThrow();
        });

        it('Should throw if value is a Boolean', function() {
            expect(setMin(true)).toThrow();
            expect(setMin(false)).toThrow();
        });

        it('Should not throw if value is -Ininity', function() {
            expect(setMin(-Infinity)).not.toThrow();
        });

        it('Should throw if value is +Ininity', function() {
            expect(setMin(Infinity)).toThrow();
        });

        it('Should set new value', function() {
            var min = -555;
            instance.setMin(min);
            expect(instance.getMin()).toEqual(min);
            expect(instance.getMin()).not.toEqual(MIN);
        });

        it('Should throw if min value is greater or equal to max ', function() {
            instance = $numerus(1, 2, 3);
            expect(instance.setMin.bind(instance, 5)).toThrow();
            expect(instance.setMin.bind(instance, 3)).toThrow();
        });
    });

    describe('When setting value using .set() to be equal to or below tha min value', function() {
        var instance;
        var VALUE = 666;
        var MIN = 1;
        var listeners = {
            floor: function() {

            },
            ceil: function() {

            }
        }

        function setMin() {
            var _args = arguments;
            return function() {
                instance.setMin.call(instance, _args[0]);
            }
        }

        beforeEach(function() {
            instance = $numerus(VALUE, MIN);
            spyOn(listeners, 'floor');
            spyOn(listeners, 'ceil');

            instance.addListener('floor', listeners.floor);
            instance.addListener('ceil', listeners.ceil);
        });

        it('Should emit "floor" event if value was set to MIN value', function() {
            instance.set(MIN);
            expect(listeners.floor).toHaveBeenCalled();
        });

        it('Should not emit "ceil" event if value was set to MIN value', function() {
            instance.set(MIN);
            expect(listeners.ceil).not.toHaveBeenCalled();
        });

        it('Should emit "floor" event if value was set to below MIN value', function() {
            instance.set(MIN - 1);
            expect(listeners.floor).toHaveBeenCalled();
        });

        it('Should not emit "ceil" event if value was set to below MIN value', function() {
            instance.set(MIN - 1);
            expect(listeners.ceil).not.toHaveBeenCalled();
        });
    });
})
