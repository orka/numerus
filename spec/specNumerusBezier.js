define(['lib/number', 'p!-curves'], function($numerus, $curves) {
    'use strict';

    describe('When making a Numerus component with optional Curve argument', function() {
        var SEED = 555;
        var VALUE = SEED * Math.random();
        var MIN = -SEED;
        var MAX = SEED + 1;
        //simple maker closure
        function make(curve) {
            return $numerus.bind(null, VALUE, MIN, MAX, curve);
        }

        it('Should not throw if "curve" is null|undefined', function() {
            expect(make()).not.toThrow();
            expect(make(null)).not.toThrow();
        });

        it('Should throw if value is Function|Object|Array', function() {
            expect(make(make)).toThrow();
            expect(make({})).toThrow();
            expect(make([])).toThrow();
        });

        it('Should throw if value is String|Empty/not', function() {
            expect(make('')).toThrow();
            expect(make('asd')).toThrow();
        });

        it('Should throw if value is a Boolean', function() {
            expect(make(true)).toThrow();
            expect(make(false)).toThrow();
        });

        it('Should throw if value is +-Ininity', function() {
            expect(make(Infinity)).toThrow();
            expect(make(-Infinity)).toThrow();
        });

        it('Should not throw if value is an instance of bezier', function() {
            var _curve = $curves.bezier();
            expect(make(_curve)).not.toThrow();
        });
    });

    describe('When using "setCurve" API', function() {
        var SEED = 555;
        var VALUE = SEED * Math.random();
        var MIN = -SEED;
        var MAX = SEED + 1;
        var instance;
        //simple maker closure
        function setCurve(curve) {
            return instance.setCurve.bind(instance, curve);
        }

        beforeEach(function() {
            instance = $numerus(VALUE, MIN, MAX, $curves.bezier());
        })

        it('Should throw if "curve" is null|undefined', function() {
            expect(setCurve()).toThrow();
            expect(setCurve(null)).toThrow();
        });

        it('Should throw if value is Function|Object|Array', function() {
            expect(setCurve(setCurve)).toThrow();
            expect(setCurve({})).toThrow();
            expect(setCurve([])).toThrow();
        });

        it('Should throw if value is String|Empty/not', function() {
            expect(setCurve('')).toThrow();
            expect(setCurve('asd')).toThrow();
        });

        it('Should throw if value is a Boolean', function() {
            expect(setCurve(true)).toThrow();
            expect(setCurve(false)).toThrow();
        });

        it('Should throw if value is +-Ininity', function() {
            expect(setCurve(Infinity)).toThrow();
            expect(setCurve(-Infinity)).toThrow();
        });

        it('Should not throw if value is an instance of bezier', function() {
            var _curve = $curves.bezier();
            expect(setCurve(_curve)).not.toThrow();
        });

        it('Should dereference listeners and curve previously set', function() {
            var _curve = $curves.bezier();
            var _crrentCurve = instance.__curve__;
            setCurve(_curve)();
            expect(_curve === _crrentCurve).toEqual(false);
            expect(_curve === instance.__curve__).toEqual(true);
        });

        it('Should not throw if .removeCurve is called without curve being set up in the first place', function() {
            instance = $numerus(VALUE, MIN, MAX);
            expect(instance.removeCurve).not.toThrow();
        });

        it('Should return self when .removeCurve is called without curve being set up in the first place', function() {
            instance = $numerus(VALUE, MIN, MAX);
            expect(instance.removeCurve()).toEqual(instance);
        });

        it('Should return self when .removeCurve is called with curve being set up in the first place', function() {
            expect(instance.removeCurve()).toEqual(instance);
        });
    });

    describe('When using "set" and "get" API while curve assigned at the end of curve', function() {
        var SEED = 555;
        var VALUE = SEED * Math.random();
        var MIN = -SEED;
        var MAX = SEED + 1;
        var NEW_SET_VALUE = VALUE / 2;
        var DURATION = 100;
        var instance;

        var listeners = {
            start: function() {},
            end: function() {},
            complete: function() {}
        };


        beforeEach(function(done) {
            instance = $numerus(VALUE, MIN, MAX, $curves.bezier(null, DURATION));
            //
            spyOn(listeners, 'start');
            spyOn(listeners, 'end');
            spyOn(listeners, 'complete');
            //
            instance.addListener('curveStart', listeners.start);
            instance.addListener('curveEnd', listeners.end);
            instance.addListener('curveComplete', listeners.complete);

            instance.set(NEW_SET_VALUE);

            setTimeout(done, DURATION * 2);
        });

        it('Should emit "start", "end" but not "complete" events if the .get was only called once after expiry', function(done) {
            instance.get();
            expect(listeners.start).toHaveBeenCalled();
            expect(listeners.end).toHaveBeenCalled();
            expect(listeners.complete).not.toHaveBeenCalled();
            done();
        });

        it('Should emit "start", "end" and "complete" events if the .get was only called more than once after expiry', function(done) {
            instance.get();
            instance.get();
            expect(listeners.start).toHaveBeenCalled();
            expect(listeners.end).toHaveBeenCalled();
            expect(listeners.complete).toHaveBeenCalled();
            done();
        });

        it('Should have .get() value to equal to .set(' + NEW_SET_VALUE + ') value after "end" event', function(done) {
            expect(instance.get()).toEqual(NEW_SET_VALUE);
            done();
        });

        it('Should have get() value to equal to .set(' + NEW_SET_VALUE + ') value after "complete" event', function(done) {
            instance.get()
            expect(instance.get()).toEqual(NEW_SET_VALUE);
            done();
        });
    });

    describe('When using "set" and "get" API while curve assigned in the middle of curve', function() {
        var SEED = 555;
        var VALUE = SEED * Math.random();
        var MIN = -SEED;
        var MAX = SEED + 1;
        var NEW_SET_VALUE = VALUE / 2;
        var DURATION = 100;
        var instance;

        var listeners = {
            start: function() {},
            end: function() {},
            complete: function() {}
        };


        beforeEach(function(done) {
            instance = $numerus(VALUE, MIN, MAX, $curves.bezier(null, DURATION));
            //
            spyOn(listeners, 'start');
            spyOn(listeners, 'end');
            spyOn(listeners, 'complete');
            //
            instance.addListener('curveStart', listeners.start);
            instance.addListener('curveEnd', listeners.end);
            instance.addListener('curveComplete', listeners.complete);

            instance.set(NEW_SET_VALUE);

            setTimeout(done, DURATION / 2);
        });

        it('Should emit "start" but not the "end" and "complete" events if the .get() was called before curve reaches its end', function(done) {
            instance.get();
            expect(listeners.start).toHaveBeenCalled();
            expect(listeners.end).not.toHaveBeenCalled();
            expect(listeners.complete).not.toHaveBeenCalled();
            done();
        });

        it('Should have .get() value to not be equal to .set(' + NEW_SET_VALUE + ') value before "end" event', function(done) {
            expect(instance.get()).not.toEqual(NEW_SET_VALUE);
            done();
        });

        it('Should have .get() value to not be equal to .set(' + NEW_SET_VALUE + ') value before "complete" event', function(done) {
            instance.get()
            expect(instance.get()).not.toEqual(NEW_SET_VALUE);
            done();
        });
    });
});
