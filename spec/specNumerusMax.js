define(['lib/number'], function($numerus) {
    'use strict';

    describe('When making a Numerus component with optional Max argument', function() {
        var VALUE = Math.random();
        var MIN = -555;
        //simple maker closure
        function make() {
            return $numerus.bind(null, VALUE, MIN, arguments[0]);
        }

        it('Should not throw if min is null|undefined', function() {
            expect(make()).not.toThrow();
            expect(make(null)).not.toThrow();
        });

        it('Should throw if  min is Function|Object|Array', function() {
            expect(make(make)).toThrow();
            expect(make({})).toThrow();
            expect(make([])).toThrow();
        });

        it('Should throw if value is String|Empty/not', function() {
            expect(make('')).toThrow();
            expect(make('asd')).toThrow();
        });

        it('Should throw if value is a Boolean', function() {
            expect(make(true)).toThrow();
            expect(make(false)).toThrow();
        });

        it('Should throw if value is -Ininity', function() {
            expect(make(-Infinity)).toThrow();
        });

        it('Should not throw if value is +Ininity', function() {
            expect(make(Infinity)).not.toThrow();
        });

        it('Should not throw if value is +-Int or +-Float', function() {
            expect(make(MIN + 1)).not.toThrow();
            expect(make(MIN + 1.1)).not.toThrow();
            expect(make(0)).not.toThrow();
        });

        it('Should throw if max value is smaller or equals to min', function() {
            expect(make(MIN)).toThrow();
            expect(make(MIN - 1)).toThrow();
            expect(make(MIN - 1.1)).toThrow();
        });

        it('Should set value to be no more than a set max', function() {
            var value = 111;
            var min = -555;
            var max = 1; // must be less than value for this spec test
            var instance = $numerus(value, min, max);
            expect(instance.get()).toEqual(max);
        });

        it('Should deconstruct object', function() {
            var instance = $numerus(1, 2, 3);
            expect(instance.deconstructor.bind(instance)).not.toThrow();
        });
    });

    describe('When accessing Max', function() {
        var instance;
        var VALUE = 111;
        var MIN = -666;
        var MAX = 666;

        beforeEach(function() {
            instance = $numerus(VALUE, MIN, MAX);
        });

        it('Should be defined', function() {
            expect(instance.getMax).toBeDefined();
        });

        it('Should return initially assigned value', function() {
            expect(instance.getMax()).toEqual(MAX);
        });
    });

    describe('When using "setMax" API', function() {
        var instance;
        var VALUE = 111;
        var MIN = -666;
        var MAX = 666;

        function setMax() {
            var _args = arguments;
            return function() {
                instance.setMax.call(instance, _args[0]);
            }
        }

        beforeEach(function() {
            instance = $numerus(VALUE);
        });

        it('Should be defined', function() {
            expect(instance.setMax).toBeDefined();
        });

        it('Should throw if value is null|undefined', function() {
            expect(setMax()).toThrow();
            expect(setMax(null)).toThrow();
        });

        it('Should throw if value is Function|Object|Array', function() {
            expect(setMax(setMax)).toThrow();
            expect(setMax({})).toThrow();
            expect(setMax([])).toThrow();
        });

        it('Should throw if value is String|Empty/not', function() {
            expect(setMax('')).toThrow();
            expect(setMax('asd')).toThrow();
        });

        it('Should throw if value is a Boolean', function() {
            expect(setMax(true)).toThrow();
            expect(setMax(false)).toThrow();
        });

        it('Should not throw if value is +Ininity', function() {
            expect(setMax(Infinity)).not.toThrow();
        });

        it('Should throw if value is -Ininity', function() {
            expect(setMax(-Infinity)).toThrow();
        });

        it('Should set new value', function() {
            var max = 555;
            instance.setMax(max);
            expect(instance.getMax()).toEqual(max);
            expect(instance.getMax()).not.toEqual(MAX);
        });
    });

    describe('When setting value using .set() to be equal to or above tha max value', function() {
        var instance;
        var VALUE = 666;
        var MAX = 1000;
        var MIN = 1;
        var listeners = {
            floor: function() {

            },
            ceil: function() {

            }
        }

        function setMin() {
            var _args = arguments;
            return function() {
                instance.setMin.call(instance, _args[0]);
            }
        }

        beforeEach(function() {
            instance = $numerus(VALUE, MIN, MAX);
            spyOn(listeners, 'floor');
            spyOn(listeners, 'ceil');

            instance.addListener('floor', listeners.floor);
            instance.addListener('ceil', listeners.ceil);
        });

        it('Should emit "ceil" event if value was set to MAX value', function() {
            instance.set(MAX);
            expect(listeners.ceil).toHaveBeenCalled();
        });

        it('Should not emit "floor" event if value was set to MAX value', function() {
            instance.set(MAX);
            expect(listeners.floor).not.toHaveBeenCalled();
        });

        it('Should emit "ceil" event if value was set to above MAX value', function() {
            instance.set(MAX + 1);
            expect(listeners.ceil).toHaveBeenCalled();
        });

        it('Should not emit "floor" event if value was set to above MAX value', function() {
            instance.set(MAX + 1);
            expect(listeners.floor).not.toHaveBeenCalled();
        });
    });
})
