define(['lib/number'], function($numerus) {
    'use strict';


    describe('When making a Numerus component with a manditory argument "value"', function() {
        function make() {
            var _args = arguments;
            return function() {
                $numerus.apply(null, _args);
            }
        }
        it('Should be defined', function() {
            expect($numerus).toBeDefined();
        });

        it('Should throw if value is null|undefined', function() {
            expect(make()).toThrow();
            expect(make(null)).toThrow();
        });

        it('Should throw if value is Function|Object|Array', function() {
            expect(make(make)).toThrow();
            expect(make({})).toThrow();
            expect(make([])).toThrow();
        });

        it('Should throw if value is String|Empty/not', function() {
            expect(make('')).toThrow();
            expect(make('asd')).toThrow();
        });

        it('Should throw if value is a Boolean', function() {
            expect(make(true)).toThrow();
            expect(make(false)).toThrow();
        });

        it('Should throw if value is +-Ininity', function() {
            expect(make(Infinity)).toThrow();
            expect(make(-Infinity)).toThrow();
        });

        it('Should not throw if value is +-Int or +-Float', function() {
            expect(make(1)).not.toThrow();
            expect(make(1.1)).not.toThrow();
            expect(make(-1)).not.toThrow();
            expect(make(-1.1)).not.toThrow();
            expect(make(0)).not.toThrow();
        });
    });

    describe('When using "get" API', function() {
        var instance;
        var VALUE = Math.random();
        beforeEach(function() {
            instance = $numerus(VALUE);
        });

        it('Should be defined', function() {
            expect(instance.get).toBeDefined();
        });

        it('Should return initially assigned value', function() {
            expect(instance.get()).toEqual(VALUE);
        });
    });

    describe('When using "set" APIs', function() {
        var instance;
        var VALUE = Math.random();

        function set() {
            var _args = arguments;
            return function() {
                instance.set.apply(instance, _args);
            }
        }

        beforeEach(function() {
            instance = $numerus(VALUE);
        });

        it('Should be defined', function() {
            expect(instance.set).toBeDefined();
        });

        it('Should throw if value is null|undefined', function() {
            expect(set()).toThrow();
            expect(set(null)).toThrow();
        });

        it('Should throw if value is Function|Object|Array', function() {
            expect(set(set)).toThrow();
            expect(set({})).toThrow();
            expect(set([])).toThrow();
        });

        it('Should throw if value is String|Empty/not', function() {
            expect(set('')).toThrow();
            expect(set('asd')).toThrow();
        });

        it('Should throw if value is a Boolean', function() {
            expect(set(true)).toThrow();
            expect(set(false)).toThrow();
        });

        it('Should throw if value is +-Ininity', function() {
            expect(set(Infinity)).toThrow();
            expect(set(-Infinity)).toThrow();
        });

        it('Should set new value', function() {
            var value = Math.random();
            instance.set(value);
            expect(instance.get()).toEqual(value);
            expect(instance.get()).not.toEqual(VALUE);
        });

        it('Should return self', function() {
            var value = Math.random();
            expect(instance.set(value)).toEqual(instance);
        });
    });

    describe('When using the "reset" APIs', function() {
        var instance;
        var VALUE = Math.random();

        function set() {
            var _args = arguments;
            return function() {
                instance.apply(instance, _args);
            }
        }

        beforeEach(function() {
            instance = $numerus(VALUE);
        });

        it('Should be defined', function() {
            expect(instance.reset).toBeDefined();
        });

        it('Should not throw', function() {
            expect(instance.reset.bind(instance)).not.toThrow();
        });

        it('Should set calue to original value after .set is called with a diferent value that of original', function() {
            var value = Math.random();
            instance.set(value);
            instance.reset();
            expect(instance.get()).toEqual(VALUE);
            expect(instance.get()).not.toEqual(value);
        });
    })
})
